package com.andriy;

import com.andriy.view.View;

public class Main {
    public static void main(String[] args) {
        new View().show();
    }
}
