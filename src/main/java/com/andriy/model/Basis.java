package com.andriy.model;

import java.util.Scanner;

public class Basis {
    Scanner sc = new Scanner(System.in);
    private String crust;
    private String cheese;
    private Sauce sauce;
    private String city;

    public Basis() {
    }

    public Basis(String crust, String cheese, Sauce sauce) {
        this.crust = crust;
        this.cheese = cheese;
        this.sauce = sauce;
    }
    public void createBasis(){
        System.out.println("Input city : ");
        city = sc.nextLine().toUpperCase();
        if (city.equals(City.KYIV.toString())){
            crust = "crust";
            cheese = "mocarella";
            sauce = Sauce.tomato;
        }else if (city.equals(City.LVIV.toString())){
            crust = "crust";
            cheese = "mocarella";
            sauce = Sauce.pesto;
        }else if (city.equals(City.DNIPRO.toString())){
            crust = "crust";
            cheese = "mocarella";
            sauce = Sauce.marinara;
        }
    }

    @Override
    public String  toString() {
        return "Basis{" +
                "crust='" + crust + '\'' +
                ", cheese='" + cheese + '\'' +
                ", sauce=" + sauce +
                '}';
    }
}
