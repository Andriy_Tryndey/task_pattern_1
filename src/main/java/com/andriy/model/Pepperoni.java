package com.andriy.model;

public class Pepperoni implements Create {
    private Basis basis;
    private String tomato;
    private String salami;
    private Enum sauce;
    private boolean crustThin;
    private int number;
    private boolean box;

    public Pepperoni() {
    }

    public Pepperoni(Basis basis, String tomato, String salami, Enum sauce) {
        this.basis = basis;
        this.tomato = tomato;
        this.salami = salami;
        this.sauce = sauce;
    }

    @Override
    public void prepare() {
        basis = new Basis();
        basis.createBasis();
        tomato = "tomato";
        salami = "salami";
        sauce = Sauce.plum;
    }

    @Override
    public void bake() {
        crustThin = false;
    }

    @Override
    public void cut() {
        number = 6;
    }

    @Override
    public void box() {
        box = true;
    }

    public Basis getBasis() {
        return basis;
    }

    public Pepperoni setBasis(Basis basis) {
        this.basis = basis;
        return this;
    }

    public String getTomato() {
        return tomato;
    }

    public Pepperoni setTomato(String tomato) {
        this.tomato = tomato;
        return this;
    }

    public String getSalami() {
        return salami;
    }

    public Pepperoni setSalami(String salami) {
        this.salami = salami;
        return this;
    }

    public Enum getSauce() {
        return sauce;
    }

    public Pepperoni setSauce(Enum sauce) {
        this.sauce = sauce;
        return this;
    }

    public boolean isCrustThin() {
        return crustThin;
    }

    public Pepperoni setCrustThin(boolean crustThin) {
        this.crustThin = crustThin;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Pepperoni setNumber(int number) {
        this.number = number;
        return this;
    }

    public boolean isBox() {
        return box;
    }

    public Pepperoni setBox(boolean box) {
        this.box = box;
        return this;
    }

    @Override
    public String toString() {
        return "Pepperoni{" +
                "basis=" + basis +
                ", tomato='" + tomato + '\'' +
                ", salami='" + salami + '\'' +
                ", sauce=" + sauce +
                ", crustThin=" + crustThin +
                ", number=" + number +
                ", box=" + box +
                '}';
    }
}
