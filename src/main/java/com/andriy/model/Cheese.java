package com.andriy.model;

public class Cheese implements Create {
    private Basis basis;
    private String parmesan;
    private String dorblu;
    private String emmental;
    private String oregano;
    private String basil;
    private boolean crustThin;
    private int number;
    private boolean box;

    public Cheese() {
    }

    public Cheese(Boolean crustThin) {
        this.crustThin = crustThin;
    }

    public Cheese(Basis basis, String parmesan, String dorblu, String emmental, String oregano, String basil) {
        this.basis = basis;
        this.parmesan = parmesan;
        this.dorblu = dorblu;
        this.emmental = emmental;
        this.oregano = oregano;
        this.basil = basil;
    }

    @Override
    public void prepare() {
        basis = new Basis();
        basis.createBasis();
        parmesan = "parmesan";
        dorblu = "dorblu";
        emmental = "emmental";
        oregano = "oregano";
        basil = "basil";
    }

    @Override
    public void bake() {
        crustThin = true;
    }

    @Override
    public void cut() {
        number = 8;
    }

    @Override
    public void box() {
        box = true;
    }

    public int getNumber() {
        return number;
    }

    public Cheese setNumber(int number) {
        this.number = number;
        return this;
    }

    public boolean isBox() {
        return box;
    }

    public Cheese setBox(boolean box) {
        this.box = box;
        return this;
    }

    public Basis getBasis() {
        return basis;
    }

    public Cheese setBasis(Basis basis) {
        this.basis = basis;
        return this;
    }

    public String getParmesan() {
        return parmesan;
    }

    public Cheese setParmesan(String parmesan) {
        this.parmesan = parmesan;
        return this;
    }

    public String getDorblu() {
        return dorblu;
    }

    public Cheese setDorblu(String dorblu) {
        this.dorblu = dorblu;
        return this;
    }

    public String getEmmental() {
        return emmental;
    }

    public Cheese setEmmental(String emmental) {
        this.emmental = emmental;
        return this;
    }

    public String getOregano() {
        return oregano;
    }

    public Cheese setOregano(String oregano) {
        this.oregano = oregano;
        return this;
    }

    public String getBasil() {
        return basil;
    }

    public Cheese setBasil(String basil) {
        this.basil = basil;
        return this;
    }

    public boolean isCrustThin() {
        return crustThin;
    }

    public Cheese setCrustThin(boolean crustThin) {
        this.crustThin = crustThin;
        return this;
    }

    @Override
    public String toString() {
        return "Cheese{" +
                "basis=" + basis +
                ", parmesan='" + parmesan + '\'' +
                ", dorblu='" + dorblu + '\'' +
                ", emmental='" + emmental + '\'' +
                ", oregano='" + oregano + '\'' +
                ", basil='" + basil + '\'' +
                ", crustThin=" + crustThin +
                ", number=" + number +
                ", box=" + box +
                '}';
    }
}
