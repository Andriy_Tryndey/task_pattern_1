package com.andriy.model;

public interface Create {
    void prepare();

    void bake();

    void  cut();

    void box();
}
