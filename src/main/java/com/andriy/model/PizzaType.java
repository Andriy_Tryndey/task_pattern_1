package com.andriy.model;

public enum PizzaType {
    Cheese, Veggie, Clam, Pepperoni
}
