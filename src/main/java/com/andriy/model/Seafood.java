package com.andriy.model;

public class Seafood implements Create{
    private Basis basis;
    private String shrimps;
    private String midie;
    private String rapans;
    private boolean crustThin;
    private int number;
    private boolean box;

    public Seafood() {
    }

    public Seafood(Basis basis, String shrimps, String midie, String rapans, boolean crustThin, int number, boolean box) {
        this.basis = basis;
        this.shrimps = shrimps;
        this.midie = midie;
        this.rapans = rapans;
        this.crustThin = crustThin;
        this.number = number;
        this.box = box;
    }

    @Override
    public void prepare() {
        basis = new Basis();
        basis.createBasis();
        shrimps = "shrimps";
        midie = "midie";
        rapans = "rapans";
    }

    @Override
    public void bake() {
        crustThin = true;
    }

    @Override
    public void cut() {
        number = 8;
    }

    @Override
    public void box() {
        box = true;
    }

    public Basis getBasis() {
        return basis;
    }

    public Seafood setBasis(Basis basis) {
        this.basis = basis;
        return this;
    }

    public String getShrimps() {
        return shrimps;
    }

    public Seafood setShrimps(String shrimps) {
        this.shrimps = shrimps;
        return this;
    }

    public String getMidie() {
        return midie;
    }

    public Seafood setMidie(String midie) {
        this.midie = midie;
        return this;
    }

    public String getRapans() {
        return rapans;
    }

    public Seafood setRapans(String rapans) {
        this.rapans = rapans;
        return this;
    }

    public boolean isCrustThin() {
        return crustThin;
    }

    public Seafood setCrustThin(boolean crustThin) {
        this.crustThin = crustThin;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Seafood setNumber(int number) {
        this.number = number;
        return this;
    }

    public boolean isBox() {
        return box;
    }

    public Seafood setBox(boolean box) {
        this.box = box;
        return this;
    }

    @Override
    public String toString() {
        return "Seafood{" +
                "basis=" + basis +
                ", shrimps='" + shrimps + '\'' +
                ", midie='" + midie + '\'' +
                ", rapans='" + rapans + '\'' +
                ", crustThin=" + crustThin +
                ", number=" + number +
                ", box=" + box +
                '}';
    }
}
