package com.andriy.model;

public enum Sauce {
    marinara, plum, tomato, pesto
}
