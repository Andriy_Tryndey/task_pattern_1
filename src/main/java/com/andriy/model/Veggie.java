package com.andriy.model;

import java.util.Scanner;

public class Veggie implements Create {
    private Basis basis;
    private String onion;
    private String mushroom;
    private String oliva;
    private String tomato;
    private String cheese;
    private String pepper;
    private boolean crustThin;
    private int number;
    private boolean box;

    public Veggie() {
    }

    public Veggie(Basis basis, String onion, String mushroom, String oliva, String tomato, String cheeseGauda, String pepper, boolean crustThin, int number, boolean box) {
        this.basis = basis;
        this.onion = onion;
        this.mushroom = mushroom;
        this.oliva = oliva;
        this.tomato = tomato;
        this.cheese = cheese;
        this.pepper = pepper;
        this.crustThin = crustThin;
        this.number = number;
        this.box = box;
    }

    @Override
    public void prepare() {
        basis = new Basis();
        basis.createBasis();
        onion = "onion";
        mushroom = "mushroom";
        oliva = "oliva";
        tomato = "tomato";
        cheese = "cheeseGauda";
        pepper = "bulgarianPepper";
    }

    @Override
    public void bake() {
        crustThin = true;
    }

    @Override
    public void cut() {
        number = 10;
    }

    @Override
    public void box() {
        box = false;
    }

    public Basis getBasis() {
        return basis;
    }

    public Veggie setBasis(Basis basis) {
        this.basis = basis;
        return this;
    }

    public String getOnion() {
        return onion;
    }

    public Veggie setOnion(String onion) {
        this.onion = onion;
        return this;
    }

    public String getMushroom() {
        return mushroom;
    }

    public Veggie setMushroom(String mushroom) {
        this.mushroom = mushroom;
        return this;
    }

    public String getOliva() {
        return oliva;
    }

    public Veggie setOliva(String oliva) {
        this.oliva = oliva;
        return this;
    }

    public String getTomato() {
        return tomato;
    }

    public Veggie setTomato(String tomato) {
        this.tomato = tomato;
        return this;
    }

    public String getCheese() {
        return cheese;
    }

    public Veggie setCheese(String cheese) {
        this.cheese = cheese;
        return this;
    }

    public String getPepper() {
        return pepper;
    }

    public Veggie setPepper(String pepper) {
        this.pepper = pepper;
        return this;
    }

    public boolean isCrustThin() {
        return crustThin;
    }

    public Veggie setCrustThin(boolean crustThin) {
        this.crustThin = crustThin;
        return this;
    }

    public int getNumber() {
        return number;
    }

    public Veggie setNumber(int number) {
        this.number = number;
        return this;
    }

    public boolean isBox() {
        return box;
    }

    public Veggie setBox(boolean box) {
        this.box = box;
        return this;
    }

    @Override
    public String toString() {
        return "Veggie{" +
                "basis=" + basis +
                ", onion='" + onion + '\'' +
                ", mushroom='" + mushroom + '\'' +
                ", oliva='" + oliva + '\'' +
                ", tomato='" + tomato + '\'' +
                ", cheese='" + cheese + '\'' +
                ", pepper='" + pepper + '\'' +
                ", crustThin=" + crustThin +
                ", number=" + number +
                ", box=" + box +
                '}';
    }
}
