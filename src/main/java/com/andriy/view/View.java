package com.andriy.view;

import com.andriy.factory.Pizza;
import com.andriy.factory.PizzaCreate;
import com.andriy.model.*;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    Scanner sc = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public View(){
        menu = new LinkedHashMap<>();

        menu.put("1", "1. Order pizza : Pepperoni");
        menu.put("2", "2. Order pizza : Cheese");
        menu.put("3", "3. Order pizza : Seafood");
        menu.put("4", "4. Order pizza : Veggie");
        menu.put("Q", "exit");

        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        Pizza pizza = new PizzaCreate();
        Pepperoni pepperony1 = (Pepperoni) pizza.make(PizzaType.Pepperoni);
        System.out.println(pepperony1);
    }

    private void pressButton2() {
        Pizza pizza = new PizzaCreate();
        Cheese cheese1 = (Cheese) pizza.make(PizzaType.Cheese);
        System.out.println(cheese1);
    }

    private void pressButton3() {
        Pizza pizza = new PizzaCreate();
        Seafood seafood1 = (Seafood) pizza.make(PizzaType.Clam);
        System.out.println(seafood1);
    }

    private void pressButton4() {
        Pizza pizza = new PizzaCreate();
        Veggie veggie = (Veggie) pizza.make(PizzaType.Veggie);
        System.out.println(veggie);
    }

    public void show(){
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Select menu point");
            keyMenu = sc.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){}
        }while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        System.out.println("\nMenu");
        for (String str : menu.values()){
            System.out.println(str);
        }
    }
}

