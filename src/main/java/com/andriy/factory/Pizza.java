package com.andriy.factory;

import com.andriy.model.Create;
import com.andriy.model.PizzaType;

public abstract class Pizza {
    protected abstract Create createPizza(PizzaType pizzaType);

    public Create make(PizzaType pizzaType) {
        Create pizza = createPizza(pizzaType);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();
        return pizza;
    }
}
