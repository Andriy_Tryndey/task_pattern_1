package com.andriy.factory;

import com.andriy.model.*;

public class PizzaCreate extends Pizza {
    @Override
    protected Create createPizza(PizzaType pizzaType) {
        Create create = null;

        if (pizzaType == PizzaType.Cheese) {
            create = new Cheese();
        }else if (pizzaType == PizzaType.Veggie) {
            create = new Veggie();
        }else if (pizzaType == PizzaType.Clam) {
            create = new Seafood();
        }else if (pizzaType == PizzaType.Pepperoni) {
            create = new Pepperoni();
        }
        return create;
    }
}
